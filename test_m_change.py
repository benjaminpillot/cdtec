# -*- coding: utf-8 -*-

""" Module summary description.

More detailed description.
"""
from collections import Counter

import numpy as np
import pandas as pd

from cdtec.base import _multi_change, count_nb_changes, single_change_detection, change_intensity
from cdtec.utils import compress

dates = pd.date_range("01/09/2018", "12/24/2018", freq="12D")
dates = np.asarray([int(d) for d in dates.strftime("%Y%m%d")])
# arrays = np.concatenate([np.random.normal(15, 5, (len(dates)//4, 50, 50)),
#                          np.random.normal(2, 3, (len(dates)//4, 50, 50)),
#                          np.random.normal(8, 4, (len(dates)//4, 50, 50)),
#                          np.random.normal(12, 2, (len(dates)//4 + len(dates) % 4, 50, 50))])
arrays = np.concatenate([np.random.normal(15, 5, (len(dates)//2, 50, 50)),
                         np.random.normal(2, 3, (len(dates)//2 + len(dates) % 2, 50, 50))])

# arrays = np.random.normal(15, 2, (len(dates), 50, 50))

single_change = single_change_detection(arrays, dates, 1, 200, no_data=0)
# single_change = np.random.choice([20180320, 20180626, 20180918, 20181113], size=(1, 1))

# test = change_intensity(arrays, dates, .95, 500, no_data=-999)

test = np.asarray(_multi_change(single_change, arrays, dates, 1, 500, no_data=0))
test = compress(test)

# changes = test.transpose().reshape((test.shape[1] * test.shape[2], test.shape[0]))

test2 = count_nb_changes(arrays, dates, 1, 500, -999)

is_duplicate = np.zeros(single_change.shape)
nb_changes = np.zeros(single_change.shape)

for i in range(test.shape[1]):
    for j in range(test.shape[2]):
        series = test[:, i, j]
        nb_changes[i, j] = np.sum(series != 0)
        if [item for item, count in Counter(series).items() if count > 1 and item != 0]:
            is_duplicate[i, j] = 1

print(is_duplicate)