# -*- coding: utf-8 -*-

""" Module summary description.

More detailed description.
"""
import os

from cdtec.base import change_tendency, single_change_detection, multi_change_detection, \
    count_nb_changes, change_intensity

from cdtec.main import cdtec_apply

# Change directory paths depending on your own configuration
input_dir = "/home/benjamin/Documents/DATA/RADAR/"
out_dir = "/home/benjamin/Documents/DATA/RADAR/CHANGE/"

# Here, call run() function with the following arguments:
# * dir: input directory where to find the images
# * output_file: output file name for change detection GeoTiff
# * threshold: confidence interval threshold (between 0 and 1)
# * max_samples: max number of bootstrap samples
# * nb_processes: number of parallel processes
# * data_type: data type for output raster
# * no_data: no data value in output raster
# * progress_bar: if True, do not display progress bar
fhandles = [change_intensity]
data_type = ["float32"]
polarizations = ["VH", "VV"]
thresholds = [.95, .99, 1]

for polarization in polarizations:
    in_dir = os.path.join(input_dir, polarization)
    cdtec_apply(fhandles, in_dir, out_dir,
                thresholds, max_samples=200,
                out_file_prefix=polarization,
                data_types=data_type,
                nb_processes=20, window_size=20,
                no_data=-999)
