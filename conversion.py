# -*- coding: utf-8 -*-

""" Module summary description.

More detailed description.
"""
from pyrasta.raster import Raster


def db_to_linear(images):
    """ Convert raster radar images from db to linear

    Parameters
    ----------
    images: list[str]

    Returns
    -------

    """
    return [10 ** (Raster(img) / 10) for img in images]
